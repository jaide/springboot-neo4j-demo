# springboot集成neo4j使用示例

## 目录

- 安装neo4j
- Neo4j与springBoot简单整合
  1. 建立一个springBoot项目
  2. 引入neo4j的pom
  3. 建立一个Person的实体类
  4. 建立PersonRepository
  5. 建立关系实体类Friend
  6. 建立关系接口FriendRepository
  7. application.properties配置文件中建立数据库连接
  8. 单元测试

## **安装neo4j**

​	下载安装neo4j，下载地址：[https://neo4j.com/download/](https://link.zhihu.com/?target=https%3A//neo4j.com/download/)，选择社区服务器下相应版本下载即可，社区版免费，不支持集群模式，企业版需要发付费。

## Neo4j与springBoot简单整合

### 建立一个springBoot项目

新建一个springboot项目，我这里的版本是2.3.0。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/142346_75618cbe_495940.png "微信截图_20200527102319.png")

### 引入neo4j的pom

上一步新建springboot项目是选择 Spring Data Neo4j,会自动引入neo4j的依赖.

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-neo4j</artifactId>
</dependency>
```

### 建立一个Person的实体类

新建person实体类，包含id、姓名和年龄字段，这里使用了lombok.Data注解，如果没有使用lombok的同学请自行添加get,set。

```java
@NodeEntity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private String  name;
    private Integer age;
}
```

### 建立PersonRepository

新建PersonRepository接口继承Neo4jRepository，默认提供基本的增删改查，这里添加了findFriendByPerson和findByName方法，后面会讲到。

```java
public interface PersonRepository extends Neo4jRepository<Person, Long> {
    /**
     * 返回某个人的所有朋友
     * @param name
     * @return
     */
    @Query("MATCH p =(n:Person)-[r:FRIEND]->(m:Person) WHERE m.name=$name RETURN n")
    List<Person> findFriendByPerson(String name);
    /**
     * 根据名字查找
     * @param name
     * @return
     */
    Person findByName(String name);
}
```

### 建立关系实体类Friend

建立朋友关系实体类，包含创建时间和备注添加好友来源，以及指向关系起点personFrom用@StartNode注解，指向关系终点personTo用@EndNode注解，相当于A（起点）向B（终点）发起好友申请，有一个指向关系。具体请了解图数据结构相关知识。

```java
@RelationshipEntity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Friend {
    @Id
    @GeneratedValue
    private Long id;
    private Date createTime;
    private String remark;

    @StartNode
    private Person personFrom;
    @EndNode
    private Person personTo;

    public Friend(Person personFrom, Person personTo, String remark) {
        this.personFrom = personFrom;
        this.personTo = personTo;
        this.remark = remark;
        this.createTime = new Date();
    }
}
```

### 建立关系接口FriendRepository

新建FriendRepository接口继承Neo4jRepository

```java
public interface FriendRepository  extends Neo4jRepository<Friend, Long> {
}
```

### application.properties配置文件中建立数据库连接

使用Neo4j安装时设置的用户名和密码

```properties
spring.data.neo4j.uri=bolt://localhost:7687
spring.data.neo4j.username=neo4j
spring.data.neo4j.password=*****
```

### 单元测试

简单的增删改查，findByName和findFriendByPerson为扩展方法，通过@Query注解可以自定义查询语句，通过$param可以引入自定义参数。
执行addNodeTest通过图形界面我们可以看到如图1已经添加成功一个节点
执行updateNodeTest通过图形界面我们可以看到如图2 age从18变成了19
重复执行addNodeRSTest并修改name添加指向jamie节点的好友关系，如图3所示。


```java
 /**
  * 添加节点
  */
@Test
public void addNodeTest(){
    Person person=new Person();
    person.setName("jamie");
    person.setAge(18);
    personRepository.save(person);
}
/**
 * 删除节点
 */
@Test
public void deleteNodeTest(){
    personRepository.deleteById(3L);
}

/**
 * 修改节点
 * 修改节点
 */
@Test
public void updateNodeTest(){
    Person person=new Person();
    person.setName("jamie");
    person.setAge(19);
    person.setId(3L);
    personRepository.save(person);
}

/**
 * 添加朋友关系

 */
@Test
public void addNodeRSTest(){
    Person person=new Person();
    person.setName("rose3");
    person.setAge(18);
    Person rose = personRepository.save(person);
    Person jamie = personRepository.findByName("jamie");
    Friend friend=new Friend(rose,jamie,"附近的人");
    friendRepository.save(friend);
}

/**
 * 查找某人所有朋友
 */
@Test
public void findFriendByPersonTest(){
    List<Person> friends = personRepository.findFriendByPerson("jamie");
    friends.forEach(System.out::println);
}
```
图一

![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/142444_cbc6b5dc_495940.png "微信截图_20200527105521.png")


图二

![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/142454_25f1084e_495940.png "微信截图_20200527111936.png")



图三

![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/142504_fdb35058_495940.png "微信截图_20200527114227.png")

