package com.xsnet.neo4jdemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.*;

import java.util.Date;

/**
 * @program: neo4j-demo
 * @description:
 * @author: Jamie.shi
 * @create: 2020-05-27 10:43
 **/
@RelationshipEntity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Friend {
    @Id
    @GeneratedValue
    private Long id;
    private Date createTime;
    private String remark;

    @StartNode
    private Person personFrom;
    @EndNode
    private Person personTo;

    public Friend(Person personFrom, Person personTo, String remark) {
        this.personFrom = personFrom;
        this.personTo = personTo;
        this.remark = remark;
        this.createTime = new Date();
    }
}
