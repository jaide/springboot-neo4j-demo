package com.xsnet.neo4jdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jamie.shi
 */
@SpringBootApplication
public class Neo4jDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Neo4jDemoApplication.class, args);
    }

}
